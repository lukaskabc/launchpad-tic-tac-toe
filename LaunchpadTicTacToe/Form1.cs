﻿using IntelOrca.Launchpad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaunchpadTicTacToe
{
    public partial class Form1 : Form
    {

        enum State
        {
            one = 1,
            two = 2,
            nil = 0
        }

        LaunchpadDevice device;

        State[,] Pool = new State[8,8];

        bool CurP = true;



        public Form1()
        {
            InitializeComponent();

            for(int i = 0; i<8; i++)
            {
                for (int a = 0; a < 8; a++)
                {
                    Pool[i, a] = State.nil;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            device = new LaunchpadDevice();
            device.DoubleBuffered = true;

            device.ButtonPressed += Launchpad_ButtonPressed;

            device.DoubleBuffered = false;

            resetLight();

            SwitchP();
        }

        void SwitchP()
        {
            if (!CurP)
            {
                foreach (ToolbarButton b in (ToolbarButton[])Enum.GetValues(typeof(ToolbarButton)))
                {
                    device.GetButton(b).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Off);
                }
            } else
            {
                foreach (ToolbarButton b in (ToolbarButton[])Enum.GetValues(typeof(ToolbarButton)))
                {
                    device.GetButton(b).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Full);
                }
            }
            CurP = !CurP;
        }

        private void Launchpad_ButtonPressed(object sender, ButtonPressEventArgs e)
        {
            if(e.Type == ButtonType.Grid)
            {
                if(Pool[e.X, e.Y] == State.nil)
                {
                    Pool[e.X, e.Y] = CurP ? State.two : State.one;
                    if(CurP) device[e.X, e.Y].SetBrightness(ButtonBrightness.Full, ButtonBrightness.Off);
                    else device[e.X, e.Y].SetBrightness(ButtonBrightness.Off, ButtonBrightness.Full);
                    CheckWin();
                }
            }
        }

        void CheckWin()
        {
            ButtonBrightness c = ButtonBrightness.Full;
            bool win = false;
            for (int i = 0; i < 8; i++)
            {
                for (int a = 0; a < 8; a++)
                {
                    State s = Pool[i, a];
                    if (s == State.nil) continue;
                    try
                    {
                        if (s == Pool[i, a+1] && s == Pool[i, a + 2] && s == Pool[i, a + 3])
                        {
                            device[i, a].SetBrightness(c, c);
                            device[i, a+1].SetBrightness(c, c);
                            device[i, a+2].SetBrightness(c, c);
                            device[i, a+3].SetBrightness(c, c);
                            win = true;

                        } 
                        if (s == Pool[i, a - 1] && s == Pool[i, a - 2] && s == Pool[i, a - 3])
                        {
                            device[i, a].SetBrightness(c, c);
                            device[i, a - 1].SetBrightness(c, c);
                            device[i, a - 2].SetBrightness(c, c);
                            device[i, a - 3].SetBrightness(c, c);
                            win = true;
                        } 
                        if (s == Pool[i+1, a + 1] && s == Pool[i+2, a + 2] && s == Pool[i+3, a + 3])
                        {
                            device[i, a].SetBrightness(c, c);
                            device[i+1, a + 1].SetBrightness(c, c);
                            device[i+2, a + 2].SetBrightness(c, c);
                            device[i+3, a + 3].SetBrightness(c, c);
                            win = true;
                        }
                        if (s == Pool[i - 1, a - 1] && s == Pool[i - 2, a - 2] && s == Pool[i - 3, a - 3])
                        {
                            device[i, a].SetBrightness(c, c);
                            device[i - 1, a - 1].SetBrightness(c, c);
                            device[i - 2, a - 2].SetBrightness(c, c);
                            device[i - 3, a - 3].SetBrightness(c, c);
                            win = true;
                        }
                        if (s == Pool[i - 1, a + 1] && s == Pool[i - 2, a + 2] && s == Pool[i - 3, a + 3])
                        {
                            device[i, a].SetBrightness(c, c);
                            device[i - 1, a + 1].SetBrightness(c, c);
                            device[i - 2, a + 2].SetBrightness(c, c);
                            device[i - 3, a + 3].SetBrightness(c, c);
                            win = true;
                        }
                        if (s == Pool[i + 1, a - 1] && s == Pool[i + 2, a - 2] && s == Pool[i + 3, a - 3])
                        {
                            device[i, a].SetBrightness(c, c);
                            device[i + 1, a - 1].SetBrightness(c, c);
                            device[i + 2, a - 2].SetBrightness(c, c);
                            device[i + 3, a - 3].SetBrightness(c, c);
                            win = true;
                        }
                        if (win) break;
                    }
                    catch (Exception e)
                    {
                        
                    }
                }
                if (win) break;
            }
            if (win)
            {
                if(CurP)
                    MessageBox.Show("Red Won");
                else
                    MessageBox.Show("Green Won");
                for (int i = 0; i < 8; i++)
                {
                    for (int a = 0; a < 8; a++)
                    {
                        Pool[i, a] = State.nil;
                    }
                }
                resetLight();
                SwitchP();
                
                return;
            }
            else
            {
                SwitchP();
            }
        }

        private void resetLight()
        {
            foreach (LaunchpadButton b in device.Buttons)
            {
                b.SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            }

            foreach(ToolbarButton b in (ToolbarButton[])Enum.GetValues(typeof(ToolbarButton)))
            {
                device.GetButton(b).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            }
            foreach (SideButton b in (SideButton[])Enum.GetValues(typeof(SideButton)))
            {
                device.GetButton(b).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            }
        }
    }
}
